var elixir = require('laravel-elixir');
var gulp = require('gulp');
var concat = require('gulp-concat');
var notify = require('gulp-notify');
var flatten = require('gulp-flatten');


gulp.task('move', function () {
    gulp.src('./resources/app/**/*.html')
        .pipe(flatten())
        .pipe(gulp.dest('./public/templates'))
        .pipe(notify("Html Compiled successfully"))
});

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


elixir(function(mix) {
    mix.task('move', 'resources/app/**/*.html');
    mix.sass('app.scss');
    mix.scriptsIn('./resources/app/');

});
