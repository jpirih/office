/**
 * Created by janko on 12/06/2017.
 */
app.controller('TaskPrioritiesController', function ($scope, TaskPrioritiesFactory) {
    $scope.title = "Task Priorities";
    $scope.priorities = TaskPrioritiesFactory.query();
    console.log(TaskPrioritiesFactory);
});