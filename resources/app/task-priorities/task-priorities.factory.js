/**
 * Created by janko on 13/06/2017.
 */

app.factory('TaskPrioritiesFactory', function ($resource) {

    return $resource('/priorities');

});