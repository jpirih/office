/**
 * Created by janko on 12/06/2017.
 */

// task priorities directive
app.directive('taskPrioritiesDirective', function () {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: '/templates/task-priorities.directive.html',
        controller: 'TaskPrioritiesController'
    };
});