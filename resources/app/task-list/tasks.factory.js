/**
* Created by janko on 13/05/2017.
*/

app.factory('TasksFactory', function ($resource) {

    return $resource('/tasks');

});