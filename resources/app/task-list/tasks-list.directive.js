/**
 * Created by janko on 10/05/2017.
 */

app.directive('tasksListDirective', function () {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: '/templates/task-list.html',
        controller: "TaskListController"
    }
});
