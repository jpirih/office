/**
 * Created by janko on 13/05/2017.
 */

app.controller("TaskListController", function ($scope, TasksFactory) {
    $scope.title = 'My tasks';
    $scope.tasks = TasksFactory.query();

});