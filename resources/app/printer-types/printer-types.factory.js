/**
 * Created by janko on 13/06/2017.
 */


app.factory('PrinterTypesFactory', function ($resource) {
    return $resource('printer-types');
});