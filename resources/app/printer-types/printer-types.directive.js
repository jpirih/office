/**
 * Created by janko on 13/06/2017.
 */

app.directive('printerTypesDirective', function () {
    return {
        restrict: 'E',
        templateUrl: '/templates/printer-types.directive.html',
        controller: 'PrinterTypesController'
    }
});