/**
 * Created by janko on 13/06/2017.
 */

app.controller('PrinterTypesController', function ($scope, PrinterTypesFactory) {
    $scope.title = "Printer Types";
    $scope.printerTypes = PrinterTypesFactory.query();

});