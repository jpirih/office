/**
 * Created by janko on 25/06/2017.
 */

app.directive("movementTypesDirective", function () {
    return {
        restrict: 'E',
        scope: true,
        controller: "MovementTypesController",
        templateUrl: '/templates/movement-types.directive.html'
    }
});
