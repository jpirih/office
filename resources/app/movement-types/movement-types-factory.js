/**
 * Created by janko on 25/06/2017.
 */

app.factory('MovementTypesFactory', function ($resource) {
    return $resource('/movement-types');
});
