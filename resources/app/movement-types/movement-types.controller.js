/**
 * Created by janko on 25/06/2017.
 */

app.controller('MovementTypesController', function ($scope, MovementTypesFactory) {
    $scope.title = "Movement Types";
    $scope.types = MovementTypesFactory.query();
});
