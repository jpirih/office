
<!-- Modal -->
<div class="modal fade" id="edit_pt{{ $transaction->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Paper Transaction</h4>
            </div>
            <!-- edit form  -->
            <div class="modal-body">
                <div class="data-box">
                    <form action="{{ route('paper-transaction-update', ['id' => $transaction->id]) }}" method="post" class="form-horizontal">
                        {{ csrf_field() }}
                        <!-- date -->
                        <div class="form-group">
                            <label for="pt_date_edit" class="control-label">Date</label>
                            <input type="text" name="date" id="pt_date_edit" value="{{ $transaction->date }}" required class="form-control">
                        </div>
                            <!-- paper type -->
                        <div class="form-group">
                            <label for="papers" class="control-label">Select Paper
                                <span class="text-muted pull-right">{{ $transaction->item->name }}</span>
                            </label>
                            <select name="papers[]" id="papers" class="form-control">
                                @foreach($papers as $paper)
                                    <option value="{{ $paper->id }}">{{ $paper->name }}</option>
                                @endforeach
                            </select>
                        </div>
                            <!-- movementType -->
                        <div class="form-group">
                            <label for="movementTypes" class="control-label">Movement Type</label>
                            <input type="text" name="movementType" id="movementTypes" value="{{ $transaction->itemMovementType->name }}" disabled class="form-control">
                        </div>
                            <!-- quantity -->
                        <div class="form-group">
                            <label for="quantity" class="control-label">Quantity</label>
                            <input type="text" name="quantity" id="quantity" value="{{ $transaction->quantity }}" disabled class="form-control">
                        </div>
                        <button type="submit" class="btn btn-success">Add</button>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


