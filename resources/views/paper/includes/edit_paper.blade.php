<!-- add paper form  -->
<!-- Modal -->
<div class="modal fade" id="editPaper{{$paper->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Paper</h4>
            </div>
            <div class="modal-body">
                <div class="data-box">
                    <form action="{{ route('paper-update', ['id' => $paper->id])}}" method="post" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name" class="control-label">Paper Name</label>
                            <input type="text" class="form-control" id="name" value="{{ $paper->name }}" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="item_type" class="control-label">Type</label>
                            <input type="text" class="form-control" id="item_type" name="item_type" value="{{ $paperType->name }}" disabled="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="description">Description</label>
                            <textarea id="description" name="description" class="form-control">
                                {{ $paper->description }}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <button type="reset" class="btn btn-info">Clear</button>
                            <button type="submit" class="btn btn-success">Update</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>