<div class="info-box">
    <h3>Paper Transactions Lists</h3>
    <table class="table table-responsive table-bordered">
        <thead>
            <tr class="table-header">
                <th>Date</th>
                <th>Paper</th>
                <th>Movement Type</th>
                <th>Quantity</th>
                <th>Current Stock Level</th>
            </tr>
        </thead>
        <tbody>
            @foreach($transactions as $transaction)
                @if($transaction->item->item_type_id === 1)
                    <tr>
                        <td>{{ $transaction->date->format('d.m.Y') }}</td>
                        <td>
                            {{ $transaction->item->name }}
                        </td>
                        <td>{{ $transaction->itemMovementType->name }}</td>
                        <td>{{ $transaction->quantity }}</td>
                        <td>{{ $transaction->stock_level }}</td>
                    </tr>
                @endif

            @endforeach
        </tbody>
    </table>
</div>

