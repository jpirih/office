<div class="data-box">
    <form action="{{ route('paper-transaction-create') }}" method="post" class="form-inline">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="pt_date" class="control-label">Date</label>
            <input type="text" name="date" id="pt_date" required class="form-control">
        </div>
        <div class="form-group">
            <label for="papers" class="control-label">Select Paper</label>
            <select name="papers[]" id="papers" class="form-control">
                @foreach($papers as $paper)
                    <option value="{{ $paper->id }}">{{ $paper->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="movementTypes" class="control-label">Movement Type</label>
            <select name="movementTypes[]" id="movementTypes" class="form-control">
                @foreach($movementTypes as $type)
                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="quantity" class="control-label">Quantity</label>
            <input type="text" name="quantity" id="quantity" required class="form-control">
        </div>
        <button type="submit" class="btn btn-success">Add</button>
    </form>
</div>