@extends('base')

@section('title')
    Paper Management
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header text-center">Paper Management</h1>
        </div>
    </div>
    <div class="row">
        @if(count($papers) !== 0)
            <div class="col-sm-4 well">
                <!-- include add_paper modal form -->
                @include('paper.includes.add_paper')
                <h2>
                    Paper Stock
                    <span class="pull-right">
                        <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addPaper">
                        Add
                    </button>
                    </span>
                </h2>
                <hr>
                @foreach($papers as $paper)
                    @include('paper.includes.edit_paper')
                    <div class="col-sm-9">
                        <h4>
                            {{ $paper->name }}
                            <span class="pull-right glyphicon glyphicon-pencil" role="button" data-toggle="modal" data-target="#editPaper{{$paper->id}}"></span>
                        </h4>
                    </div>
                    <div class="inventory-box col-sm-3">
                        <h4 class="text-center">{{ $paper->stock }}</h4>
                    </div>
                    <br>
                @endforeach
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-sm-12 well">
            <h3>Paper Transactions</h3>
                @include('paper.includes.new_paper_transaction')
            <br>
            @if( count($transactions) !== 0)
                @include('paper.includes.paper_transactions')
            @endif
        </div>
    </div>
    <!-- javascript utils for this page -->
    <script>
        $('#pt_date').datepicker({
            format:"dd.mm.yy",
            showOtherMonths: true,
            selectOtherMonths: true
        });
    </script>
@endsection