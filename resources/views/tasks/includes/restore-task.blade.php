<form action="{{ route('task-restore', ['id' => $task->id]) }}" method="post" class="option-button">
    {{ csrf_field() }}
    <button type="submit" class="btn btn-info" title="Restore Task">
        <span class="glyphicon glyphicon-refresh"></span>
        Restore
    </button>
</form>