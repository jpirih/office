
<!-- Modal -->
<div class="modal fade" id="editTask" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit {{ $task->title }}</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('task-update', ['id' => $task->id]) }}" method="post" class="form-horizontal data-box">
                    {{ csrf_field()  }}
                        <div class="form-group">
                        <label for="title" class="control-label col-sm-4">Title</label>
                        <div class="col-sm-8">
                            <input type="text" name="title" id="title" value="{{ $task->title }}" required class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="control-label col-sm-4">Description</label>
                        <div class="col-sm-8">
                            <textarea name="description" id="description" required class="form-control" cols="30" rows="5">
                                {{ $task->description  }}
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="completed" class="control-label col-sm-4">Task completed</label>
                        <div class="col-sm-8">
                            <input type="checkbox"  class="form-control" name="completed"  id="completed" @if($task->completed == true) checked @endif >
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="submit" class="btn btn-success">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>