<form action="{{ route('task-delete', ['id' => $task->id]) }}" method="post" class="option-button">
    {{ csrf_field() }}
    <button type="submit" CLASS="btn btn-danger" title="Delete Task">
        <span class="glyphicon glyphicon-trash"></span>
        Delete
    </button>
</form>