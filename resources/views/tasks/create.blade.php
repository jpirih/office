@extends('base')

@section('title')
    Add Task
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header text-center">Add New Task</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <form action="" method="post" class="form-horizontal data-box">
                {{ csrf_field()  }}
                <div class="form-group">
                    <label for="title" class="control-label col-sm-4">Title</label>
                    <div class="col-sm-8">
                        <input type="text" name="title" id="title" required class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="description" class="control-label col-sm-4">Description</label>
                    <div class="col-sm-8">
                        <textarea name="description" id="description" cols="30" rows="5" required class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="priority" class="control-label col-sm-4">Priority</label>
                    <div class="col-sm-8">
                        <select name="priorities[]" id="priority" class="form-control">
                            @foreach($priorities as $priority)
                                <option value="{{ $priority->id  }}">{{ $priority->label }} - {{ $priority->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit"  class="btn btn-success">
                            <span class="glyphicon glyphicon-plus"></span>
                            Create Task
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection