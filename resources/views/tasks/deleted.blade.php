@extends('base')

@section('title')
    Deleted tasks
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header text-center">Deleted Tasks</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-9">

            @if(count($deletedTasks) === 0)
                <div class="alert alert-info">
                    <p>
                        <span class="glyphicon glyphicon-info-sign"></span>
                        There is no deleted tasks at this time
                    </p>
                </div>
            @else
                <div class="info-box">
                    <p>
                        <span class="glyphicon glyphicon-info-sign"></span>
                        If you want to restore these tasks  click on title  to restore one at the time
                    </p>
                </div>
                <hr>
                <table class="table table-responsive table-bordered">
                    <thead>
                    <tr class="table-header">
                        <th>Title</th>
                        <th>Deleted</th>
                        <th>Created</th>
                        <th>Created By</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($deletedTasks as $task)
                        <tr>
                            <td><a href="{{ route('tasks-details', ['id' => $task->id]) }}">{{ $task->title }}</a></td>
                            <td>{{ $task->updated_at->format('d.m.Y @ H:i:s') }}</td>
                            <td>{{ $task->created_at->format('d.m.Y @ H:i:s') }}</td>
                            <td>{{ $task->user->name }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif

        </div>

    </div>
@endsection