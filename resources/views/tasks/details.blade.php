@extends('base')

@section('title')
    Details
@endsection

@section('content')
    @include('tasks.includes.edit-task')
    <!-- main title -->
    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header text-center">{{ $task->title  }} details</h1>
        </div>
    </div>
    <!-- task details -->
    <div class="row">
        <div class="col-sm-6">
            <div class="data-box">
                <span class="bold">Created</span> {{ $task->created_at->format('d.m.y@H:i:s') }}
                <span class="bold pull-right">Last update {{ $task->updated_at->format('d.m.y@H:i:s') }}</span>
                <p>
                    <span class="bold">{{ $task->priority->label }} -  {{ $task->priority->name }}</span>
                    <span class="bold pull-right"> Created by: {{ $task->user->name }} </span>
                </p>
            </div>
            <br>
            <div class="info-box">
                <h3>Task description</h3>
                <p class="bold">
                    {{ $task->description }}
                </p>
            </div>
            <br>
            <div class="well">
                <a href="{{ route('tasks-list')  }}" class="btn btn-danger">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    Back
                </a>

                @if($task->deleted !== true)
                    <a role="button" class="btn btn-info" data-toggle="modal" data-target="#editTask">
                        <span class="glyphicon glyphicon-edit"></span>
                        Edit
                    </a>
                    @include('tasks.includes.delete-task')
                @else
                    @include('tasks.includes.restore-task')
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            @if($task->completed !== true)
            @else
                @include('tasks.includes.done-task')
            @endif
        </div>
    </div>
@endsection