@extends('base')

@section('title')
    Tasks Lists
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header text-center">Task Lists</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-9">
            <tasks-list-directive></tasks-list-directive>
        </div>
        <div class="col-sm-3">
            <a href="{{ route('task-create') }}" class="btn btn-block btn-success">
                <span class="glyphicon glyphicon-plus"></span>
                Add Task
            </a>
            <h3>Options</h3>
            <a href="{{ route('deleted-tasks') }}" class="btn btn-block btn-danger">
                <span class="glyphicon glyphicon-trash"></span>
                Deleted Tasks
            </a>
        </div>
    </div>
@endsection