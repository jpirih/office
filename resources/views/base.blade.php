<!doctype html>
<html lang="en" data-ng-app="app">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jquery ui theme css -->
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- jquery -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <!-- bootstrap js -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- angular -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    <script src="/js/angular-resource.js"></script>
    <!-- my css -->
    <link rel="stylesheet" href="/css/app.css" type="text/css">
    <script src="/js/all.js"></script>

    <title>Task Manager|@yield('title')</title>
</head>
<body>
<!--  main template - top navigation  -->
<nav class="navbar navbar-inverse">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Kekec Lab</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::user())
                    <li class="active"><a href="/logout">logout <span class="sr-only">(current)</span></a></li>
                @else
                    <li class="active"><a href="/login">Login <span class="sr-only">(current)</span></a></li>
                    <li><a href="/register">Register</a></li>
                @endif
                @if(Auth::user())
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tools <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('tasks-list') }}">Task Manager</a></li>
                                <li><a href="{{ route('printing') }}">Printing Supplies</a></li>
                                <li><a href="{{ route('paper') }}">Office Supplies</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                <li><a href="/logout">Logout</a></li>
                            </ul>
                        </li>
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="container">
    @yield('content')
</div>
</body>
</html>