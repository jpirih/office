@extends('base')

@section('title')
    Home
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header text-center">Kekec Lab Tools</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8">
            <div class="info-box">
                <h3>Wellcome</h3>
                <p>
                    This application is for internall use only designed just for personal notes about what needs to be done
                    when I changed toners in my printers and paper usage.
                </p>
                <p>At the momemnt is still work in progress ... </p>

            </div>
            <br>
            <div class="well">
                <a href="{{ route('dashboard')  }}" class="btn btn-primary btn-lg">
                    <span class="glyphicon glyphicon-log-in"></span>
                    Enter
                </a>
            </div>
        </div>
        <div class="col-sm-4"></div>
    </div>
@endsection