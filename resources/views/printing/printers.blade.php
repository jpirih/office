@extends('base')

@section('title')
    Printing
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header text-center">Printing</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-9 well">
            <h3>My Printers</h3>
            @if(count($printers) === 0)
                <div class="alert alert-info">
                    <p>
                        <span class=" glyphicon glyphicon-info-sign"></span>
                        There is no Printers saved in the database at the moment.
                    </p>
                </div>
            @else
                @foreach($printers as $printer)
                    <div class="media">
                        <div class="media-left media-middle">
                            <a href="#">
                                <img class="media-object" src="{{ $printer->img }}" alt="{{$printer->name}}" height="300" width="300">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">{{$printer->name}}</h4>
                            <div class="data-box">
                                {{ $printer->printerType->name }}
                            </div>
                            <br>
                            <div class="info-box">
                                <p>
                                    {{ $printer->description }}
                                </p>
                            </div>
                            <br>
                            <div>
                                <a href="{{ route('printer-details', ['id' => $printer->id])}}" class="btn btn-primary">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                    Details
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="col-sm-3">
            <a href="{{ route('printer-create') }}" class="btn btn-block btn-success">
                <span class="glyphicon glyphicon-plus"></span>
                Add Printer
            </a>
            <br>
            <div class="inf-box">
                <h3>Utilities</h3>
                <a href="{{ route('toner-create') }}" class="btn btn-block btn-primary">
                    <span class="glyphicon glyphicon-plus"></span> Add Toner
                </a>
                <a href="{{ route('toners-list') }}" class="btn btn-block btn-default">
                    <span class="glyphicon glyphicon-list"></span> Toners
                </a>
            </div>
        </div>
    </div>
@endsection
