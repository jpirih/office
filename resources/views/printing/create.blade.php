@extends('base')

@section('title')
    Add Printer
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header text-center">Add Printer</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 data-box">
            <form action="" method="post" class="form-horizontal">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" class="control-label col-sm-4">Printer Name</label>
                    <div class="col-sm-8">
                        <input type="text" name="name" id="name" required class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="description" class="control-label col-sm-4">Description</label>
                    <div class="col-sm-8">
                        <textarea name="description" id="description" cols="30" rows="5" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="img" class="control-label col-sm-4">Image Url</label>
                    <div class="col-sm-8">
                        <input type="text" name="img" id="img" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="printer_types" class="control-label col-sm-4">Printer Type</label>
                    <div class="col-sm-8">
                        <select name="printerTypes[]" id="printer_types" CLASS="form-control">
                            @foreach($printerTypes as $type)
                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button class="btn btn-success" type="submit">
                            <span class="glyphicon glyphicon-plus"></span>
                            Add Printer
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-6"></div>
    </div>

@endsection