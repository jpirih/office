
<h3 class="text-center">Toner Replacements List</h3>
<div class="info-box text-center">
<table class="table table table-responsive table-bordered">
    <thead>
        <tr class="table-header">
            <th>Date</th>
            <th>Toner</th>
            <th>Toner Capacity</th>
            <th>Copies Made</th>
        </tr>
    </thead>
    <tbody>
        @foreach($printer->tonerReplacements as $replacement)
            @include('printing.includes.edit_toner_replacement')
            <tr>
                <td>{{ $replacement->date->format('d.m.y') }}</td>
                <td>
                    {{ $replacement->toner->name }}
                    <span class="pull-right">
                        <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#edit_tr{{$replacement->id}}">
                            Edit
                        </button>
                    </span>
                </td>
                <td>{{ $replacement->toner->capacity }}</td>
                <td>{{ $replacement->copies_count }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
</div>
