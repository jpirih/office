

<!-- Modal -->
<div class="modal fade" id="edit_tr{{ $replacement->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Toner Replacement</h4>
            </div>
            <div class="modal-body">
                <div class="data-box">
                    <form action="{{ route('toner-replace-update', ['id' => $printer->id, 'replaceId' => $replacement->id]) }}" method="post" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="tr_date_edit" class="control-label">Date</label>
                            <input type="text" name="date" id="tr_date_edit" value="{{ $replacement->date }}" required class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="printerToners" class="control-label">Select Toner</label>
                            <select name="printerToners[]" id="printerToners" class="form-control">
                                @foreach($printerToners as $toner)
                                    <option value="{{ $toner->id }}">{{ $toner->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="copies_count" class="control-label">Copies Made</label>
                            <input type="text" name="copies_count" id="copies_count" value="{{ $replacement->copies_count }}" required class="form-control">
                        </div>
                        <button type="submit" class="btn btn-success">Save</button>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>




