<div class="data-box">
    <form action="{{ route('toner-replace-create', ['id' => $printer->id]) }}" method="post" class="form-inline">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="tr_date" class="control-label">Date</label>
            <input type="text" name="date" id="tr_date" required class="form-control">
        </div>
        <div class="form-group">
            <label for="printerToners" class="control-label">Select Toner</label>
            <select name="printerToners[]" id="printerToners" class="form-control">
                @foreach($printerToners as $toner)
                    <option value="{{ $toner->id }}">{{ $toner->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="copies_count" class="control-label">Copies Made</label>
            <input type="text" name="copies_count" id="copies_count" required class="form-control">
        </div>
        <button type="submit" class="btn btn-success">Add</button>
    </form>
</div>

