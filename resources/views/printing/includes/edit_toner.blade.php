
<!-- Modal -->
<div class="modal fade" id="edit_toner{{ $toner->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{$toner->name}}</h4>
            </div>
            <div class="modal-body">
                <div class="data-box">
                    <form action="{{ route('toner-update', ['id'=> $toner->id]) }}" method="post" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name" class=" control-label col-sm-4">Name</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{ $toner->name }}" required>
                        </div>
                        <div class="form-group">
                            <label for="description" class="control-label col-sm-4">Description</label>
                            <textarea name="description" id="description" cols="30" rows="5" class="form-control" required>
                                {{ $toner->description }}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label for="capacity" class="control-label col-sm-4">Capacity</label>
                            <input type="text" name="capacity" id="capacity" value="{{ $toner->capacity }}" required class="form-control">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">
                                Save Changes
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>