<div class="well">
    <h3>{{ $printer->name }}</h3>
    <div class="data-box">
        <p>
            <span class="bold">Descripton</span> {{ $printer->description }}
        </p>
        <p>
            <span class="bold">Printer Type</span> {{ $printer->printerType->name }}
        </p>
    </div>
    <br>
    @if(count($printer->toners) !== 0)
        <h4>Toners</h4>
        <div class="info-box">
            <table class="table table-responsive table-bordered table-condensed">
                <thead>
                <tr class="table-header">
                    <th>Toner</th>
                    <th>Capacity</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                @foreach($printer->toners as $toner)
                    @include('printing.includes.edit_toner')
                    <tr>
                        <td>
                            {{ $toner->name }}
                            <button type="button" class="btn btn-info btn-xs pull-right" data-toggle="modal" data-target="#edit_toner{{$toner->id}}">
                                Edit
                            </button>
                        </td>
                        <td>{{ $toner->capacity }}</td>
                        <td>{{ $toner->description }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <a href="{{ route('toner-create') }}" class="btn btn-primary text-right">
                <span class="glyphicon glyphicon-plus"></span>
                Add Toner
            </a>

        </div>
</div>
    @endif