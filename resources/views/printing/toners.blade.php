@extends('base')

@section('title')
    Toners List
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header text-center">List of Tonners</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            @if(count($toners) !== 0)
                <table class="table table-responsive table-bordered">
                    <thead>
                    <tr class="table-header">
                        <th>Toner</th>
                        <th>Printer</th>
                        <th>Capacity</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($toners as $toner)
                        @include('printing.includes.edit_toner')
                        <tr>
                            <td>
                                {{ $toner->name }}
                                <button type="button" class="btn btn-info btn-xs pull-right" data-toggle="modal" data-target="#edit_toner{{$toner->id}}">
                                    Edit
                                </button>
                            </td>
                            <td>
                                @foreach($toner->printers as $printer)
                                    {{ $printer->name}}
                                @endforeach
                            </td>
                            <td>{{ $toner->capacity }}</td>
                            <td>{{ $toner->description }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-info">
                    <span class="glyphicon glyphicon-info-sign"></span>
                    Currently is no toners in the database.
                </div>
            @endif
        </div>
    </div>
@endsection