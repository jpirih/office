@extends('base')

@section('title')
    Add Toner
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header text-center">Add Toner</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="data-box">
                <form action="" method="post" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name" class="control-label col-sm-4">Toner Name</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" id="name" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="control-label col-sm-4">Description</label>
                        <div class="col-sm-8">
                            <textarea name="description" id="description" class="form-control" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="capacity" class="control-label col-sm-4">Toner Capacity</label>
                        <div class="col-sm-8">
                            <input type="number" name="capacity" id="capacity" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Select Printer</label>
                        <div class="col-sm-8">
                            @foreach($printers as $printer)
                                <label>
                                    <input type="checkbox" name="printers[]" value="{{ $printer->id }}"> {{ $printer->name }}
                                </label>

                            @endforeach
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button class="btn btn-success" type="submit">
                                <span class="glyphicon glyphicon-plus"></span>
                                Add Toner
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection