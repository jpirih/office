@extends('base')

@section('title')
    Printer Details
@endsection

@section('content')
    <!-- main title -->
    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header text-center">{{ $printer->name }} Details</h1>
        </div>
    </div>
    <div class="row">
        <!-- left column printer image -->
        <div class="col-sm-4">
            <div class="">
                <img src="{{ $printer->img }}" alt="{{ $printer->name }}}" class="img img img-responsive logo-picture img-rounded">
            </div>
        </div>
        <div class="col-sm-8">
            <!-- right column printer details -->
           @include('printing.includes.printer_details')
        </div>
    </div>
    <!-- right column printer toners -->
    @if(count($printer->toners) !== 0)
    <div class="row">
        <!-- toner replacements section  -->
        <div class="col-sm-12">
            <div class="well">
                <h3 class="text-center">Add Toner Replacement</h3>
                @include('printing.includes.new_toner_replacement')
                <br>
                @if(count($printer->tonerReplacements) !== 0)
                    @include('printing.includes.toner_replacements_list')
                @endif
            </div>

        </div>
    </div>
    @endif

    <!-- javascript - jquery utils for this page  -->
    <script>
        // date picker for add toner replacemnt
        $('#tr_date').datepicker({
            format:"dd.mm.yy",
            showOtherMonths: true,
            selectOtherMonths: true
        });

        // date picker for edit toner replacement
        $('#tr_date_edit').datepicker({
            format:"dd.mm.yy",
            showOtherMonths: true,
            selectOtherMonths: true
        });
    </script>
@endsection