<div class="data-box">
    <h4>Add Printer Type</h4>
    <form action="{{ route('create-movement-type') }}" method="post" class="form-inline">
        {{ csrf_field() }}
        <div class="form-group">
            <input type="text" required name="name" id="name" placeholder="Printer Type Name" class="form-control">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success">Add</button>
        </div>
    </form>
</div>
<br>