<div class="data-box">
    <h4>Add Priority</h4>
    <form action="{{ route('priority-create') }}" method="post" class="form-inline">
        {{ csrf_field() }}
        <div class="form-group">
            <input type="text" required name="label" id="label" placeholder="Label" class="form-control">
        </div>
        <div class="form-group">
            <input type="text" required name="name" id="name" placeholder="Name" class="form-control">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success">Add</button>
        </div>
    </form>
</div>
<br>