@extends('base')

@section('title')
    Dashboard
@endsection

@section('content')
    <!-- main title -->
    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header text-center">Kekec Lab Manager Dashboard</h1>
        </div>
    </div>
    <!-- navigation -->
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 well text-center">
            <div class="btn-group text-center">
                <a href="{{ route('tasks-list') }}" role="button" class="btn btn-lg btn-primary">Task Manager</a>
                <a href="{{ route('paper') }}" role="button" class="btn btn-lg btn-primary">Office Supplies</a>
                <a href="{{ route('printing') }}" role="button" class="btn btn-lg btn-primary">Printing supplies</a>
            </div>
        </div>
    </div>
    <!-- utilities  lists  -->
    <div class="row">
        <div class=" col-sm-6">
            @include('dashboard.includes.new_task_priority')
            <task-priorities-directive></task-priorities-directive>
        </div>
        <div class="col-sm-6">
            @include('dashboard.includes.new_printer_type')
            <printer-types-directive></printer-types-directive>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            @include('dashboard.includes.new_movement_type')
            <movement-types-directive></movement-types-directive>
        </div>
    </div>
@endsection
