/**
 * Created by janko on 10/05/2017.
 */
// test this is test

var app = angular.module('app', ['ngResource']);


/**
 * Created by janko on 13/06/2017.
 */

app.controller('PrinterTypesController', function ($scope, PrinterTypesFactory) {
    $scope.title = "Printer Types";
    $scope.printerTypes = PrinterTypesFactory.query();

});
/**
 * Created by janko on 13/06/2017.
 */

app.directive('printerTypesDirective', function () {
    return {
        restrict: 'E',
        templateUrl: '/templates/printer-types.directive.html',
        controller: 'PrinterTypesController'
    }
});
/**
 * Created by janko on 13/06/2017.
 */


app.factory('PrinterTypesFactory', function ($resource) {
    return $resource('printer-types');
});
/**
 * Created by janko on 25/06/2017.
 */

app.factory('MovementTypesFactory', function ($resource) {
    return $resource('/movement-types');
});

/**
 * Created by janko on 25/06/2017.
 */

app.controller('MovementTypesController', function ($scope, MovementTypesFactory) {
    $scope.title = "Movement Types";
    $scope.types = MovementTypesFactory.query();
});

/**
 * Created by janko on 25/06/2017.
 */

app.directive("movementTypesDirective", function () {
    return {
        restrict: 'E',
        scope: true,
        controller: "MovementTypesController",
        templateUrl: '/templates/movement-types.directive.html'
    }
});

/**
 * Created by janko on 12/06/2017.
 */
app.controller('TaskPrioritiesController', function ($scope, TaskPrioritiesFactory) {
    $scope.title = "Task Priorities";
    $scope.priorities = TaskPrioritiesFactory.query();
    console.log(TaskPrioritiesFactory);
});
/**
 * Created by janko on 12/06/2017.
 */

// task priorities directive
app.directive('taskPrioritiesDirective', function () {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: '/templates/task-priorities.directive.html',
        controller: 'TaskPrioritiesController'
    };
});
/**
 * Created by janko on 13/06/2017.
 */

app.factory('TaskPrioritiesFactory', function ($resource) {

    return $resource('/priorities');

});
/**
 * Created by janko on 13/05/2017.
 */

app.controller("TaskListController", function ($scope, TasksFactory) {
    $scope.title = 'My tasks';
    $scope.tasks = TasksFactory.query();

});
/**
 * Created by janko on 10/05/2017.
 */

app.directive('tasksListDirective', function () {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: '/templates/task-list.html',
        controller: "TaskListController"
    }
});

/**
* Created by janko on 13/05/2017.
*/

app.factory('TasksFactory', function ($resource) {

    return $resource('/tasks');

});
//# sourceMappingURL=all.js.map
