<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemMovementType extends Model
{
    protected $table = 'item_movement_types';

    // one-to-many connection with table items_movements table
    public function movements()
    {
        return $this->belongsTo('App\ItemMovement');
    }
}
