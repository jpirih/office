<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrinterType extends Model
{
    protected $table = 'printer_types';

    // on-to-many connection with printers table
    public function printers(){
        return $this->hasMany('App\Printer');
    }
}
