<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    // items table Model
    protected $table = 'items';

    // one-to-many connection with table items_movements
    public function transactions()
    {
        return $this->hasMany('App\ItemMovement');
    }

    // one-to-many relationship with table item_types
    public function itemType()
    {
        return $this->belongsTo('App\ItemType');
    }
}
