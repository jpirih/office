<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Printer extends Model
{
    protected $table = 'printers';

    // one-to-many connection with table printer_types
    public function printerType(){
        return $this->belongsTo('App\PrinterType');
    }

    // many-to-many connection - toners with pivot printers_toners
    public function toners()
    {
        return $this->belongsToMany('App\Toner', 'printers_toners');
    }

    // one-to-many relationship with table toner_replacements
    public function tonerReplacements()
    {
        return $this->hasMany('App\TonerReplacement');
    }
}
