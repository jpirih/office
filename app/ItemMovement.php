<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemMovement extends Model
{
    // items_movements table
    protected $table = 'items_movements';

    // one-to-many connection with items table
    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    // one-to-many connection with table item_movement_types
    public function itemMovementType()
    {
        return $this->belongsTo('App\ItemMovementType');
    }
}
