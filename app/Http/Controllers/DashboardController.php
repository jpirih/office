<?php

namespace App\Http\Controllers;

use App\ItemMovementType;
use App\PrinterType;
use App\Priority;
use Illuminate\Support\Facades\Request;


class DashboardController extends Controller
{
     // Dasboard controller contructor

    public function __construct()
    {
        $this->middleware('auth');
    }

    // admin dashboard index
    public function dashboard()
    {
        return view('dashboard.index');
    }

    // task-priorities  json of priorities table angular list showing
    public function priorities()
    {
        $priorities = Priority::all();
        return $priorities;
    }

    // printer types  json of priorities table angular list showing
    public function printerTypes()
    {
        $printerTypes = PrinterType::all();
        return $printerTypes;
    }

    // movement types json of movement_types table angular list showing
    public function movementTypes()
    {
        $movementTypes = ItemMovementType::all();
        return $movementTypes;
    }

    // create new task priority
    public function createTaskPriority()
    {
        $label = Request::get('label');
        $label = strtoupper($label);
        $name = Request::get('name');

        $priority = new Priority();
        $priority->label = $label;
        $priority->name = $name;
        $priority->save();

        return redirect()->back();

    }

    // create new printer type
    public function createPrinterType()
    {
        $name = Request::get('name');
        $type = new PrinterType();
        $type->name = $name;
        $type->save();

        return redirect() ->back();
    }


    // create new printer type
    public function createMovementType()
    {
        $name = Request::get('name');
        $type = new ItemMovementType();
        $type->name = $name;
        $type->save();

        return redirect() ->back();
    }


}
