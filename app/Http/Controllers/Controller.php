<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    // custom function to convert text from datepicker to laravel date
    public function kekecDate($dateString)
    {
        $dateConv = strtotime($dateString);
        $date = date('Y-m-d', $dateConv);
        return $date;
    }

    // convert db string timestamp to carbon date for formatting
    public function dbStringToDate($collection)
    {
        foreach ($collection as $item)
        {
            $item->date = Carbon::createFromFormat("Y-m-d H:i:s", $item->date);
        }
        return $collection;
    }
}
