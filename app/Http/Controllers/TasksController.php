<?php

namespace App\Http\Controllers;

use App\Priority;
use App\Task;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class TasksController extends Controller
{
    // tasks controller constructor
    public function __construct()
    {
        $this->middleware('auth');
    }

    // task manager main page

    public function index()
    {
        return view('tasks.index');
    }

    // all user task list page
    public function tasks()
    {

        $user = Auth::user();
        $tasks = Task::where([['user_id', '=', $user->id], ['deleted', '=', false]])->orderBy('title', 'asc')->get();
        return $tasks;
    }

    // task details
    public function taskDetails($taskId)
    {
        $task = Task::find($taskId);
        return view('tasks.details', ['task' => $task ]);
    }

    // add new  task
    public function addTask()
    {
        $priorities = Priority::all();
        return view('tasks.create', ['priorities' => $priorities]);
    }

    // create new task
    public function createTask()
    {
        $user = Auth::user();

        $title = Request::get('title');
        $description = Request::get('description');
        $priorities = Request::get('priorities');
        $priority = $priorities[0];

        $newTask = new Task();

        $newTask->title = $title;
        $newTask->description = $description;
        $newTask->priority_id = $priority;
        $newTask->user_id = $user->id;
        $newTask->save();

        return redirect('tasks-list');
    }

    // update task
    public function taskUpdate($taskId)
    {
        $task = Task::find($taskId);

        $title = Request::get('title');
        $description = Request::get('description');
        $completed = Request::get('completed');

        $taskCompleted = false;
        if($completed =="on")
        {
            $taskCompleted = true;
        }

        $task->title = $title;
        $task->description = $description;
        $task->completed = $taskCompleted;
        $task->save();

        return redirect()->back();
    }

    // task  soft delete
    public function taskDelete($taskId)
    {
        $removedTask = Task::find($taskId);
        $removedTask->deleted = true;
        $removedTask->save();

        return redirect('tasks-list');
    }

    // task restore
    public function taskRestore($taskId)
    {
        $task = Task::find($taskId);
        $task->deleted = false;
        $task->save();
        return redirect('tasks-list');
    }

    // deleted tasks list
    public function deletedTasksList()
    {
        $user = Auth::user();
        $deletedTasks = Task::where([['user_id', '=', $user->id], ['deleted', '=', true]])->get();
        return view('tasks.deleted', ['deletedTasks' => $deletedTasks]);
    }
}
