<?php

namespace App\Http\Controllers;

use App\Printer;
use App\PrinterType;

use App\Http\Requests;
use App\Toner;
use App\TonerReplacement;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;

class PrintingController extends Controller
{
    // printing controller constructor
    public function __construct()
    {
        $this->middleware('auth');
    }

    // list of all printers
    public function printers()
    {
        $printers = Printer::where('deleted', '=', false)->get();
        $data = ['printers' => $printers];
        return view('printing.printers', $data);
    }

    // add new printer
    public function addPrinter()
    {
        $printerTypes = PrinterType::where('deleted', '=', false)->get();

        $data = ['printerTypes' => $printerTypes];
        return view('printing.create', $data);
    }

    // create printer
    public function createPrinter()
    {
        $name = Request::get('name');
        $description = Request::get('description');
        $img = Request::get('img');
        $printerTypes = Request::get('printerTypes');
        $type = $printerTypes[0];

        $newPrinter = new Printer();
        $newPrinter->name = $name;
        $newPrinter->description = $description;
        $newPrinter->img = $img;
        $newPrinter->printer_type_id = $type;
        $newPrinter->save();

        return redirect(route('printing'));
    }

    // printer details
    public function printerDetails($printerId)
    {
        $printer = Printer::find($printerId);
        $printerToners = $printer->toners;
        // date format -> slo date
        foreach ($printer->tonerReplacements as $replacement){
            $replacement->date = Carbon::createFromFormat('Y-m-d', $replacement->date);
        }

        return view('printing.printer_details', ['printer' => $printer, 'printerToners' => $printerToners]);
    }

    // create_toner_replacement
    public function createTonerReplacement($printerId)
    {
        $date = Request::get('date');
        $date = $this->kekecDate($date);
        $printerToners = Request::get('printerToners');
        $tonerId = $printerToners[0];
        $copiesCount = Request::get('copies_count');

        $replacement = new TonerReplacement();
        $replacement->date = $date;
        $replacement->printer_id = $printerId;
        $replacement->toner_id = $tonerId;
        $replacement->copies_count = $copiesCount;
        $replacement->save();

        return redirect()->back();
    }

    // update_toner_replacemnt
    public function updateTonerReplacement($printerId, $replacementId)
    {
        $date = Request::get('date');
        $date = $this->kekecDate($date);
        $printerToners = Request::get('printerToners');
        $tonerId = $printerToners[0];
        $copiesCount = Request::get('copies_count');

        $replacement = TonerReplacement::find($replacementId);
        $replacement->date = $date;
        $replacement->printer_id = $printerId;
        $replacement->toner_id = $tonerId;
        $replacement->copies_count = $copiesCount;
        $replacement->save();

        return redirect()->back();
    }

    // toners list - list of all toners
    public  function tonersList()
    {
        $toners = Toner::where('deleted', '=', false)->get();
        return view('printing.toners', ['toners' => $toners]);
    }

    // add toner
    public function addToner()
    {
        $printers = Printer::all();
        return view('printing.toner_create', ['printers' => $printers]);
    }

    // crate toner
    public function createToner()
    {
        $name = Request::get('name');
        $description = Request::get('description');
        $capacity = Request::get('capacity');
        $printers = Request::get('printers');

        $toner = new Toner();
        $toner->name = $name;
        $toner->description = $description;
        $toner->capacity = $capacity;
        $toner->save();

        foreach ($printers as $item)
        {
            $printer = Printer::find($item);
            $printer->toners()->attach($toner->id);
        }
        return redirect(route('printing'));
    }

    // update toner
    public function updateToner($tonerId)
    {
        $name = Request::get('name');
        $description = Request::get('description');
        $capacity = Request::get('capacity');

        $toner = Toner::find($tonerId);
        $toner->name = $name;
        $toner->description = $description;
        $toner->capacity = $capacity;
        $toner->save();

        return redirect()->back();
    }



}
