<?php

namespace App\Http\Controllers;

use App\Item;
use App\ItemMovement;
use App\ItemMovementType;
use App\ItemType;
use Illuminate\Support\Facades\Request;


class PaperSuppliesController extends Controller
{
    // paper supplies controller constructor
    public function __construct()
    {
        return $this->middleware('auth');
    }

    // paper management index
    public function paper()
    {
        $papers = Item::where('item_type_id', '=', 1)->get();
        $transactions = ItemMovement::all();
        $this->dbStringToDate($transactions);
        $movementTypes = ItemMovementType::all();
        $paperType = ItemType::find(1);

        $data = [
            'papers' => $papers,
            'transactions' => $transactions,
            'movementTypes' => $movementTypes,
            'paperType' => $paperType
            ];
        return view('paper.paper', $data );
    }

    // add paper to db
    public function createPaper()
    {
        // data from web form
        $name = Request::get('name');
        $description = Request::get('description');
        $type = 1;

        // saving to db
        $newPaper = new Item();
        $newPaper->name = $name;
        $newPaper->description = $description;
        $newPaper->item_type_id = $type;
        $newPaper->save();

        return redirect()->back();
    }

    // edit paper
    public function updatePaper($paperId)
    {
        $name = Request::get('name');
        $description = Request::get('description');

        $paper = Item::find($paperId);
        $paper->name = $name;
        $paper->description = $description;
        $paper->save();

        return redirect()->back();

    }

    // create new paper transaction
    public function createPaperTransaction()
    {
        $date = Request::get('date');
        // text to date converstion   and proper format
        $date = $this->kekecDate($date);

        $papers = Request::get('papers');
        $paper = $papers[0];
        $movementTypes = Request::get('movementTypes');
        $movementType = $movementTypes[0];
        $quantity = Request::get('quantity');

        $item = Item::find($paper);
        // change item (paper) stock depending on movement type
        switch ($movementType)
        {
            case 1:
                $item->stock = $quantity;
                $item->save();
                break;
            case 2:
                $item->stock = $item->stock + $quantity;
                $item->save();
                break;
            case 3:
                $item->stock = $item->stock - $quantity;
                $item->save();
                break;
            case 4:
                $item->stock = $item->stock + $quantity;
                $item->save();
                break;
            case 5:
                $item->stock = $item->stock - $quantity;
                $item->save();
                break;
        }
        $movement = new ItemMovement();
        $movement->date = $date;
        $movement->item_id = $paper;
        $movement->item_movement_type_id = $movementType;
        $movement->quantity = $quantity;
        $movement->stock_level = $item->stock;
        $movement->save();

        return redirect()->back();
    }


}
