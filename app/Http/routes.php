<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'web'], function (){
    Route::get('/', 'MainController@index')->name('home');
});

Route::group(['middleware' => 'web'], function (){
    Route::auth();

    // dashboard
    Route::get('dashboard', 'DashboardController@dashboard')->name('dashboard');
    // json data -> angular lists - saved resources/app/**/
    Route::get('priorities', 'DashboardController@priorities')->name('priorities');
    Route::get('printer-types', 'DashboardController@printerTypes')->name('printer-types');
    Route::get('movement-types', 'DashboardController@movementTypes')->name('movement-types');

    // dashboard add  utilities to list Dashboard controller
    Route::post('printer-types/create', 'DashboardController@createPrinterType')->name('create-printer-type');
    Route::post('movement-types/create', 'DashboardController@createMovementType')->name('create-movement-type');
    Route::post('priorities/create', 'DashboardController@createTaskPriority')->name('priority-create');

});

Route::group(['middleware' => 'web'], function (){
    Route::auth();
    // task manager
    Route::get('tasks', 'TasksController@tasks')->name('tasks');
    Route::get('tasks/create', 'TasksController@addTask')->name('task-create');
    Route::post('tasks/create', 'TasksController@createTask');
    Route::get('tasks/{id}', 'TasksController@taskDetails')->name('tasks-details');
    Route::post('tasks/{id}/update', 'TasksController@taskUpdate')->name('task-update');
    Route::post('tasks/{id}/delete', 'TasksController@taskDelete')->name('task-delete');
    Route::post('tasks/{id}/restore', 'TasksController@taskRestore')->name('task-restore');
    Route::get('tasks-list', 'TasksController@index')->name('tasks-list');
    Route::get('deleted-tasks', 'TasksController@deletedTasksList')->name('deleted-tasks');
});

Route::group(['middleware' => 'web'], function (){
    Route::auth();
    // paper management office supplies routes
    Route::get('paper-management', 'PaperSuppliesController@paper')->name('paper');
    Route::post('paper-management/transactions/create', 'PaperSuppliesController@createPaperTransaction')->name('paper-transaction-create');
    Route::post('paper-management/papers/create', 'PaperSuppliesController@createPaper')->name('paper-create');
    Route::post('paper-management/papers/{id}/update', 'PaperSuppliesController@updatePaper')->name('paper-update');

});

Route::group(['middleware' => 'web'], function (){
    Route::auth();
    // printing
    Route::get('printers', 'PrintingController@printers')->name('printing');
    Route::get('printers/create', 'PrintingController@addPrinter')->name('printer-create');
    Route::post('printers/create', 'PrintingController@createPrinter');
    Route::get('printers/{id}', 'PrintingController@printerDetails')->name('printer-details');
    Route::post('printers/{id}/create/toner-replace', 'PrintingController@createTonerReplacement')->name('toner-replace-create');
    Route::post('printers/{id}/update/toner-replace/{replaceId}', 'PrintingController@updateTonerReplacement')->name('toner-replace-update');
    Route::get('toners/create', 'PrintingController@addToner')->name('toner-create');
    Route::get('toners-list', 'PrintingController@tonersList')->name('toners-list');
    Route::post('toners/create', 'PrintingController@createToner');
    Route::post('toners/{id}/update', 'PrintingController@updateToner')->name('toner-update');
});
