<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemType extends Model
{
    // table item_types
    protected $table = 'item_types';

    // one-to-many connection with table items
    public function items()
    {
        return $this->hasMany('App\Item');
    }
}
