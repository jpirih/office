<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TonerReplacement extends Model
{
    // toner replacements table model
    protected $table = 'toner_replacements';

    // one-to-many connection with table printers
    public function printer()
    {
        return $this->belongsTo('App\Printer');
    }

    // one-to-many connection with table toners
    public function toner()
    {
        return $this->belongsTo('App\Toner');
    }
}
