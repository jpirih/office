<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Toner extends Model
{
    // table toners
    protected $table = 'toners';

    // many-to-many relationship printers_toners pivot table
    public function printers()
    {
        return $this->belongsToMany('App\Printer', 'printers_toners');

    }

    // one-to-many relationship with table toner_replacements
    public function tonerReplacements()
    {
        return $this->hasMany('App\TonerReplacement');
    }

}
