<?php

use App\Priority;
use Illuminate\Database\Seeder;

class PrioritiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $firstPriority = new Priority();

        $firstPriority->label = 'P1';
        $firstPriority->name = 'High Priority';
        $firstPriority->deleted = false;
        $firstPriority->save();

        $secondPriority = new Priority();

        $secondPriority->label = 'P2';
        $secondPriority->name = 'Normal Priority';
        $secondPriority->deleted = false;
        $secondPriority->save();

        $thirdPriority = new Priority();

        $thirdPriority->label = 'P3';
        $thirdPriority->name = 'Low Priority';
        $thirdPriority->deleted = false;
        $thirdPriority->save();

    }
}
