<?php

use App\PrinterType;
use Illuminate\Database\Seeder;

class PrinterTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // add printer_types

        $printerType1 = new PrinterType();
        $printerType1->name = 'black-white laser';
        $printerType1->deleted = false;
        $printerType1->save();

        $printerType2 = new PrinterType();
        $printerType2->name = 'black-white mf';
        $printerType2->deleted = false;
        $printerType2->save();

        $printerType3 = new PrinterType();
        $printerType3->name = 'color laser';
        $printerType3->deleted = false;
        $printerType3->save();


    }
}
