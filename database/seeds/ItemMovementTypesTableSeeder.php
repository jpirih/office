<?php

use App\ItemMovementType;
use Illuminate\Database\Seeder;

class ItemMovementTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $movementType1 = new ItemMovementType();
        $movementType1->name = 'Initial state';
        $movementType1->deleted = false;
        $movementType1->save();

        $movementType2 = new ItemMovementType();
        $movementType2->name = 'Purchase';
        $movementType2->deleted = false;
        $movementType2->save();

        $movementType3 = new ItemMovementType();
        $movementType3->name = 'Consumption';
        $movementType3->deleted = false;
        $movementType3->save();

    }
}
