<?php

use App\ItemType;
use Illuminate\Database\Seeder;

class ItemTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // add item types
        $itemType1 = new ItemType();
        $itemType1->name = 'Paper';
        $itemType1->deleted = false;
        $itemType1->save();

        $itemType2 = new ItemType();
        $itemType2->name = 'Other';
        $itemType2->deleted = false;
        $itemType2->save();
    }
}
