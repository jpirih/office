<?php

use App\Item;
use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // insert into items table
        $item1 = new Item();
        $item1->name = 'Papir R-Copy A4 500/1';
        $item1->description = 'Papir A4 bel 80g/m2 500/1 5x500';
        $item1->item_type_id = 1;
        $item1->stock = 7;
        $item1->deleted = false;
        $item1->save();
    }
}
