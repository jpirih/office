<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create admin user
        $kekec = new User();
        $kekec->name = "Janko Pirih";
        $kekec->email = "janko.pirih@gmail.com";
        $kekec->password = "kekec123";
        $kekec->admin = true;
        $kekec->save();
    }
}
