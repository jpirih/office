<?php

use App\Task;
use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $task1 = new Task();
        $task1->title = "Test Task";
        $task1->description = "thi si test task";
        $task1->priority_id = 1;
        $task1->user_id = 1;
        $task1->completed = false;
        $task1->deleted = false;

        $task1->save();

    }
}
