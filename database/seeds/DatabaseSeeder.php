<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PrioritiesTableSeeder::class);
        $this->call(PrinterTypeSeeder::class);
        $this->call(ItemTypeTableSeeder::class);
        $this->call(ItemsTableSeeder::class);
        $this->call(ItemMovementTypesTableSeeder::class);
    }
}
