<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create item_types table
        Schema::create('item_types', function (Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->string('name');
            $blueprint->boolean('deleted')->default(false);
            $blueprint->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // drop item_types table
        Schema::drop('item_types');
    }
}
