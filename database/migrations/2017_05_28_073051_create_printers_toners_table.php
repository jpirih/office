<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreatePrintersTonersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create printers_toners pivot table
        Schema::create('printers_toners', function (Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->integer('printer_id');
            $blueprint->integer('toner_id');
            $blueprint->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // drop printers_toners pivot table
        Schema::drop('printers_toners');
    }
}
