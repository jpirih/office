<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateTonerReplacementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // toner replacements table
        Schema::create('toner_replacements', function (Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->date('date');
            $blueprint->integer('printer_id');
            $blueprint->integer('toner_id');
            $blueprint->integer('copies_count');
            $blueprint->boolean('deleted')->default(false);
            $blueprint->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // toner replacements drop table
        Schema::drop('toner_replacements');
    }
}
