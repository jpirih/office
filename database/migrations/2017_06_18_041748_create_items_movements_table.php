<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create items_movement table
        Schema::create('items_movements', function (Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->dateTime('date');
            $blueprint->integer('item_id');
            $blueprint->integer('item_movement_type_id');
            $blueprint->float('quantity');
            $blueprint->float('stock_level');
            $blueprint->boolean('deleted')->defalut(false);
            $blueprint->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // drop items_movements table
        Schema::drop('items_movements');
    }
}
